# Desafio Site Mercado (PT-BR)

Este projeto foi desenvoldido  com [Angular CLI](https://github.com/angular/angular-cli) versão 8.3.2.

# Ver Demonstração

https://desafio-site-mercado.azurewebsites.net

# Rodar Local

1 - Faça o download deste repositório.

2 - Criar o banco de dados "Products" e Rodar a Web Api

(Veja como fazer isso no repositório da Api: https://bitbucket.org/luancherubim/desafio-site-mercado-api/src/master/README.md)

3 - Executar comando 'npm install' no console para instalar as dependências.

4 - Verificar Url da Api

Veja se em src/environments a variável apiUrl é a mesma da que está rodando a Api localmente.

(por padrão: https://localhost:44338/api/product/).

5 - Executar 'ng serve --open' para iniciar.

Navegar para 'http://localhost:4200/'.



# Desafio Site Mercado (ENG)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.2.

# Run Demo

https://desafio-site-mercado.azurewebsites.net

# Run on Local server

1 - Download this repository.

2 - Create Database "Products" and Run Web Api

(See how to on Api repository: https://bitbucket.org/luancherubim/desafio-site-mercado-api/src/master/README.md)

3 - Run 'npm install' on console to install all project dependencies

4 - Check Api Url

Check if src/environments apiUrl is the same of the running Api

(by default: https://localhost:44338/api/product/).

5 - Run 'ng serve --open' for a dev server.

Navigate to 'http://localhost:4200/'. 