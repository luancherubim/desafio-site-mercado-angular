import { Component, OnInit } from '@angular/core';

import { ApiService, Product } from './../services/api.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  public products: Array<Product>;

  public selectedProduct: Product = {
    productId: null,
    name: null,
    price: null,
    image: null
  };

  public noProducts: boolean;

  public isLoading = true;

  public isDeleting: boolean;

  public selectProduct(product: Product) {
    this.selectedProduct = product;
    $('#deleteModal').modal('show');
  }

  public deleteProduct(id) {
    this.isDeleting = true;
    this.api.deleteProduct(id).subscribe(
      (success) => {
        this.getProducts();
        $('#deleteModal').modal('hide');
        toastr.success('Produto deletado com sucesso.');
        this.isDeleting = false;
      },
      (error) => {
        toastr.error('Produto deletado com sucesso.');
        this.isDeleting = false;
        console.log(error);
      }
    );
  }

  public getProducts() {
    this.api.getProducts().subscribe(
      (products) => {
        this.products = products;
        if (this.products.length === 0) {
          this.noProducts = true;
        }
        this.isLoading = false;
      },
      (error) => { console.log(error); }
    );
  }

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getProducts();
  }

}
