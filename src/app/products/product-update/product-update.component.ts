import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { ApiService } from 'src/app/services/api.service';

declare const toastr: any;

@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.scss']
})
export class ProductUpdateComponent implements OnInit {

  public updateForm: FormGroup;

  public isLoading = true;

  public isUpdating = false;

  public updateProduct() {
    Object.keys(this.updateForm.controls).forEach(field => {
      const control = this.updateForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    if (this.updateForm.valid) {
      this.isUpdating = true;
      this.api.updateProduct(this.updateForm.value).subscribe(
        (success) => {
          toastr.success('Produto atualizado com sucesso.');
          this.router.navigate(['/produtos']);
          this.isUpdating = false;
        },
        (error) => {
          toastr.error('Erro ao atualizar produto.');
          this.isUpdating = false;
          console.log(error);
        }
      );
    }
  }

  public getProduct(id) {
    this.api.getProduct(id).subscribe(
      (product) => {
        this.updateForm = this.formBuilder.group({
          productId: [id],
          name: [product.name, [Validators.required]],
          price: [product.price, [Validators.required]],
          image: [product.image]
        });
        this.isLoading = false;
      },
      (error) => { console.log(error); }
    );
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {
      this.updateForm.get('image').setValue(event.target.result);
    });
    reader.readAsDataURL(file);
  }

  removeImage() {
    this.updateForm.get('image').setValue(null);
  }

  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.getProduct(parseInt(params.id, 10));
    });
  }

}
