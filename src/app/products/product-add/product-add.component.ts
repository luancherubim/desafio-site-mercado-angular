import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ApiService } from './../../services/api.service';

declare const toastr: any;

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {

  public addForm: FormGroup;

  public isAdding: boolean;

  public addProduct() {
    Object.keys(this.addForm.controls).forEach(field => {
      const control = this.addForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    if (this.addForm.valid) {
      this.isAdding = true;
      this.api.addProduct(this.addForm.value).subscribe(
        (success) => {
          toastr.success('Produto cadastrado com sucesso.');
          this.router.navigate(['/produtos']);
          this.isAdding = false;
        },
        (error) => {
          toastr.error('Erro ao cadastrar produto.');
          this.isAdding = false;
          console.log(error); }
      );
    }
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', (event: any) => {
      this.addForm.get('image').setValue(event.target.result);
    });
    reader.readAsDataURL(file);
  }

  removeImage() {
    this.addForm.get('image').setValue(null);
  }

  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      price: [null, [Validators.required]],
      image: [null]
    });
  }

}
