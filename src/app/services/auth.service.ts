import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface Login {
  Username: string;
  Password: string;
}

interface AuthResponse {
  success: string;
  error: string;
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  public authenticated: boolean;

  public isLoggedIn() {
    return this.authenticated;
  }

  public login(credencials: Login) {
    const url = 'https://dev.sitemercado.com.br/api/login';
    const body = credencials;
    const httpOptions = {
      headers: new HttpHeaders({
        ContentType: 'application/json',
        Authorization: 'Basic ' + btoa(credencials.Username + ':' + credencials.Password)
      })
    };
    return this.httpClient.post<AuthResponse>(url, body, httpOptions);
  }

  public logout() {
    this.authenticated = false;
    this.router.navigate(['/login']);
  }

  constructor(
    private router: Router,
    private httpClient: HttpClient
  ) { }

}
