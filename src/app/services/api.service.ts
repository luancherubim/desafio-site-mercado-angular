import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../environments/environment';

export interface Product {
    productId: number;
    name: string;
    price: number;
    image: string;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public apiUrl = environment.apiUrl;

  public getProducts() {
    const url = this.apiUrl;
    const httpOptions = {
      headers: new HttpHeaders({
        ContentType: 'application/json'
      })
    };
    return this.httpClient.get<Array<Product>>(url, httpOptions);
  }

  public addProduct(product: Product) {
    const url = this.apiUrl;
    const body = product;
    const httpOptions = {
      headers: new HttpHeaders({
        ContentType: 'application/json'
      })
    };
    return this.httpClient.post(url, body, httpOptions);
  }

  public deleteProduct(id) {
    const url = this.apiUrl + id;
    return this.httpClient.delete(url);
  }

  public getProduct(id) {
    const url = this.apiUrl + id;
    return this.httpClient.get<Product>(url);
  }

  public updateProduct(product: Product) {
    const url = this.apiUrl;
    const body = product;
    const httpOptions = {
      headers: new HttpHeaders({
        ContentType: 'application/json'
      })
    };
    return this.httpClient.put(url, body, httpOptions);
  }

  constructor( private httpClient: HttpClient ) { }
}
