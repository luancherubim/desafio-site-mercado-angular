import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from './../services/auth.service';

declare const $: any;
declare const toastr: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  public modalMsg: string;

  public isLoading: boolean;

  public login() {
    Object.keys(this.loginForm.controls).forEach(field => {
      const control = this.loginForm.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    if (this.loginForm.valid) {
      this.isLoading = true;
      this.auth.login(this.loginForm.value).subscribe(
        (response) => {
          if (response.success) {
            this.auth.authenticated = true;
            this.router.navigate(['']);
            this.isLoading = false;
            toastr.success('Successo ao logar.');

          } else {
            this.auth.authenticated = false;
            this.modalMsg = response.error;
            $('#loginErrorModal').modal('show');
            this.isLoading = false;
          }
        },
        (error) => {
          console.log(error);
        });
    }
  }

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      Username: [null, [Validators.required]],
      Password: [null, [Validators.required]]
    });
  }

}
