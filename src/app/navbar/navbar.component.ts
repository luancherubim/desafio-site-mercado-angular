import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { AuthService } from './../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public isProductsRoute: boolean;

  public logout() {
    this.auth.logout();
  }

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.router.events.subscribe((event: NavigationEnd) => {
      if (event.url) {
        if (event.url.split('/')[1] === 'produtos') {
          this.isProductsRoute = true;
        } else {
          this.isProductsRoute = false;
        }
      }
    });
  }
}
